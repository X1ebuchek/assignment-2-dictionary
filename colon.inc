%define DICT 0
%macro colon 2
	%ifid %2
		%2:
		dq DICT
	%else
		%error "second argument is not label"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%error "first argument is not string"
	%endif
	%define DICT %2
%endmacro