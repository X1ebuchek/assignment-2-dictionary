section .text
global find_word
extern string_equals

find_word:

	xor rax, rax
	mov rdx, rsi

	.Loop:
		add rsi, 8

		push rdi
		push rdx
		call string_equals
		pop rdx
		pop rdi

		cmp rax, 1
		je .Found

		mov rsi, [rdx]
		mov rdx, rsi

		test rdx, rdx
		je .Not_found

		jmp .Loop 

	.Found:
		mov rax, rdx
		ret

	.Not_found:
		xor rax, rax
		ret
