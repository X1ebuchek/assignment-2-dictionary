%include "lib.inc"
extern find_word

section .rodata
error_string_too_long: db "String is too long", 0xA, 0
error_key_not_found: db "Key was not found", 0xA, 0

section .bss
buffer: resb 255

section .data
%include "words.inc"

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, 255

	call read_word
	test rax, rax
	je .error_string_too_long


	mov rdi, buffer
    mov rsi, DICT


    call find_word
	test rax, rax
	je .error_key_not_found


	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi 
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
	
	.error_key_not_found:
		mov rdi, error_key_not_found
		call print_error
		mov rdi,1 
		call exit
	.error_string_too_long:
		mov rdi, error_string_too_long
        call print_error
		mov rdi,1 
		call exit
