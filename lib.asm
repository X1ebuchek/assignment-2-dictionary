section .text
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

print_error:
    push rdi
    call string_length
    pop rsi
    
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
     xor rax, rax
    xor r8, r8
    mov rax, rdi
    mov r9, 0xA
.loop:
    xor rdx, rdx
    div r9
    inc r8
    push rdx
    cmp rax, 0
    je .print_d
    jmp .loop
.print_d:
    pop rdx
    add rdx, 0x30
    mov rdi, rdx
    push rcx
    push r8
    push r9
    call print_char
    pop r9
    pop r8
    pop rcx
    dec r8
    jnz .print_d
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print_d
    neg rdi
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi

    .print_d:
        jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov r8b, byte [rdi + rax]
    cmp byte [rsi + rax], r8b
    jnz .f
    cmp byte [rdi + rax], 0
    jz .t
    cmp byte [rsi + rax], 0
    inc rax
    jnz .loop
.t:
    mov rax, 1
    ret
.f:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
	xor rax, rax
	push rdi
	mov r9, rdi
	mov r8, rsi
	.loop:
		push rcx
        push r8
        push r9
		call read_char
        pop r9
        pop r8
		pop rcx

		cmp rax, 0xA
		je .b

		cmp rax, 0x20
		je .b

		cmp rax, 0x9
		je .b

		cmp rax, 0
		je .end

		cmp rcx, r8 
		jnl .E

		mov byte[r9 + rcx], al
		inc rcx
		jmp .loop
	.b:
		cmp rcx, 0
		je .loop
	.end:
		mov byte[r9+rcx],0
		pop rax
		mov rdx, rcx 
		ret
	.E:	
		pop rax
		xor rax, rax
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    mov r11, 10
    .L:
        push rsi
        mov sil, [rdi]
	test sil, sil
	je .end
	cmp sil, 10
	je .end
	cmp sil, '0'
	jb .end
	cmp sil, '9'
	ja .end
        sub sil, 0x30
	mul r11
	add rax, rsi
	inc rdi
	pop rsi
	inc rsi
	jmp .L
    .end:
        pop rdx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi; save pointer
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .end
    mov sil, [rdi]
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, '-'
    jne .end
    neg rax 
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    push rdi
    push rsi
    push rdx
    call string_length 
    pop rdx
    pop rsi
    pop rdi    

    cmp rdx, rax
    jl .bad_end

    xor r8, r8
    xor rcx, rcx
    .loop:
        mov cl, [rdi+r8]
        mov [rsi+r8], cl
        inc r8
        cmp r8, rax
        jle .loop

    .bad_end:
        xor rax, rax
        ret
