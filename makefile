.PHONY: clean
%.o : %.asm
	nasm -f elf64 -o $@ $<

task: lib.o main.o dict.o
	ld -o $@ $+
	./task
	
clean:
	rm *.o
	rm task